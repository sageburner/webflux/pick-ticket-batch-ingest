# Pick Ticket Batch Ingest
Latest Version: 0.1.0

## Build
```
./gradlew buildDocker
```

## Run
```
docker run --network="host" \
  -e REST_BASE_URL={REST_BASE_URL} \
  -e TENANT_ID={TENANT_ID} \
  -e USER_ID={USER_ID} \
  -e FILE_NAME={FILE_NAME} \
  -v {FILE_LOCATION}:/opt/app/working-files \
  registry.gitlab.com/pick-ticket-batch-ingest:0.1.0
```
### Run Config

- REST_BASE_URL
    - The base URL for the Par Inventory environment (e.x. https://api.sageburner.com)
- TENANT_ID
    - The `tenantId` used to search for Par Inventory instances
- USER_ID
    - The `userId` used to update Par Inventory instances
- UPLOAD_FILE_LOCATION
    - The location of the file containing the item information to update
