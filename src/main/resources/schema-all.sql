DROP TABLE par_item IF EXISTS;

CREATE TABLE par_item  (
    id VARCHAR(50) NOT NULL PRIMARY KEY,
    parInventoryId VARCHAR(50),
    externalId VARCHAR(50),
    description VARCHAR(200),
    source VARCHAR(50),
    price DOUBLE PRECISION,
    note VARCHAR(500)
);
