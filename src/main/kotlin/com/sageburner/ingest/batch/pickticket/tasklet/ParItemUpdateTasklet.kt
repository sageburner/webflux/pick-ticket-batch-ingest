package com.sageburner.ingest.batch.pickticket.tasklet

import com.fasterxml.jackson.databind.ObjectMapper
import com.sageburner.ingest.batch.pickticket.mapper.ParItemRowMapper
import com.sageburner.ingest.batch.pickticket.model.ParItem
import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.ParameterizedTypeReference
import org.springframework.core.env.Environment
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component
import org.springframework.web.client.RestTemplate
import java.net.URI

@Component
class ParItemUpdateTasklet : Tasklet {

    private val log = LoggerFactory.getLogger(ParItemUpdateTasklet::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    lateinit var restTemplate: RestTemplate

    @Autowired
    lateinit var mapper: ObjectMapper

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    @Autowired
    lateinit var rowMapper: ParItemRowMapper

    override fun execute(contribution: StepContribution, chunkContext: ChunkContext): RepeatStatus {
        val parItems: List<ParItem> = jdbcTemplate.query("SELECT * FROM par_item") { rs, row ->
            rowMapper.mapRow(rs, row)
        }

        val parItemsRequest: Map<String, String> = parItems.map { it.id to it.vendor }.toMap()
        val parIdsToUpdate: Map<String, List<String>> = findPars(parItemsRequest)

        if (parIdsToUpdate.isNotEmpty()) {
            val parsToUpdate: MutableList<ParItem> = mutableListOf()
            parItems.filter { item -> parIdsToUpdate.containsKey(item.id) }.forEach { item ->
                parIdsToUpdate.get(item.id)?.forEach { parId ->
                    parsToUpdate.add(item.copy(parInventoryId = parId))
                }
            }

            updatePars(parsToUpdate)
        }

        return RepeatStatus.FINISHED
    }

    private fun findPars(parItemsRequest: Map<String, String>): Map<String, List<String>> {
        log.info("SEARCHING FOR PARS >>>")

        val baseUrl = env.getProperty("app.target.rest.baseUrl")
        val searchUrlPath = env.getProperty("app.target.rest.searchUrlPath")
        val tenantId = env.getProperty("app.target.rest.tenantId")

        val searchUrl = "$baseUrl/$searchUrlPath?tenantId=$tenantId"
        log.debug("SEARCH URL: $searchUrl")
        val headers = HttpHeaders()

        env.getProperty("app.target.rest.headers")
                .split(",").map {
            string -> string.split(":")
        }.forEach {
            slist ->
            val sarray = slist.toTypedArray()
            headers.add(sarray.get(0), sarray.get(1))
        }

        val request: HttpEntity<Map<String, String>> = HttpEntity(parItemsRequest, headers)
        log.debug("REQUEST BODY: \n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request.body))

        val respType = object: ParameterizedTypeReference<Map<String, List<String>>>(){}
        val response: ResponseEntity<Map<String, List<String>>> = restTemplate.exchange(URI(searchUrl), HttpMethod.POST, request, respType)
        log.debug("RESPONSE BODY: \n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(response.body))

        return response.body
    }



    private fun updatePars(parUpdateRequest: List<ParItem>) {
        val baseUrl = env.getProperty("app.target.rest.baseUrl")
        val editUrlPath = env.getProperty("app.target.rest.editUrlPath")
        val userId = env.getProperty("app.target.rest.userId")

        val editUrl = "$baseUrl/$editUrlPath?userId=$userId"
        log.debug("EDIT URL: $editUrl")
        val headers = HttpHeaders()

        env.getProperty("app.target.rest.headers")
                .split(",").map {
            string -> string.split(":")
        }.forEach {
            slist ->
            val sarray = slist.toTypedArray()
            headers.add(sarray.get(0), sarray.get(1))
        }

        val request: HttpEntity<List<ParItem>> = HttpEntity(parUpdateRequest, headers)
        log.debug("REQUEST BODY: \n" + mapper.writerWithDefaultPrettyPrinter().writeValueAsString(request.body))

        val respType = object: ParameterizedTypeReference<Map<String, List<String>>>(){}
        val response: ResponseEntity<Map<String, List<String>>> = restTemplate.exchange(URI(editUrl), HttpMethod.PUT, request, respType)
        log.debug("RESPONSE STATUS: " + response.statusCode)
    }

}