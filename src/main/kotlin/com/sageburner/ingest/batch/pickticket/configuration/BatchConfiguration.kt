package com.sageburner.ingest.batch.pickticket.configuration

import com.sageburner.ingest.batch.pickticket.listener.JobCompletionNotificationListener
import com.sageburner.ingest.batch.pickticket.tasklet.DataStoreCleanupTasklet
import com.sageburner.ingest.batch.pickticket.tasklet.FileRenameTasklet
import com.sageburner.ingest.batch.pickticket.tasklet.ParItemUpdateTasklet
import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.launch.support.RunIdIncrementer
import org.springframework.batch.core.launch.support.SimpleJobLauncher
import org.springframework.batch.core.repository.JobRepository
import org.springframework.batch.core.repository.support.MapJobRepositoryFactoryBean
import org.springframework.batch.support.transaction.ResourcelessTransactionManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
@EnableBatchProcessing
class BatchConfiguration {

    @Autowired
    lateinit var jobBuilderFactory: JobBuilderFactory

    @Autowired
    lateinit var stepBuilderFactory: StepBuilderFactory

    @Autowired
    lateinit var listener: JobCompletionNotificationListener

    @Autowired
    lateinit var importParItemStep: Step

    @Autowired
    lateinit var fileRenameTasklet: FileRenameTasklet

    @Autowired
    lateinit var parItemUpdateTasklet: ParItemUpdateTasklet

    @Autowired
    lateinit var dataStoreCleanupTasklet: DataStoreCleanupTasklet

    @Bean
    fun importParItemJob(): Job {
        return jobBuilderFactory.get("importParItemJob")
                .listener(listener)
                .incrementer(RunIdIncrementer())
                .flow(fileRenameStep())
                .next(importParItemStep)
                .next(parItemUpdateStep())
                .next(dataStoreCleanupStep())
                .end()
                .build()
    }

    @Bean
    fun fileRenameStep(): Step {
        return stepBuilderFactory.get("fileRenameStep")
                .tasklet(fileRenameTasklet)
                .build()
    }

    @Bean
    fun parItemUpdateStep(): Step {
        return stepBuilderFactory.get("parItemUpdateStep")
                .tasklet(parItemUpdateTasklet)
                .build()
    }

    @Bean
    fun dataStoreCleanupStep(): Step {
        return stepBuilderFactory.get("dataStoreCleanupStep")
                .tasklet(dataStoreCleanupTasklet)
                .build()
    }

    // Scheduling

    @Bean
    fun transactionManager(): ResourcelessTransactionManager {
        return ResourcelessTransactionManager()
    }

    @Bean
    @Throws(Exception::class)
    fun mapJobRepositoryFactory(
            txManager: ResourcelessTransactionManager): MapJobRepositoryFactoryBean {

        val factory = MapJobRepositoryFactoryBean(txManager)

        factory.afterPropertiesSet()

        return factory
    }

    @Bean
    @Throws(Exception::class)
    fun jobRepository(
            factory: MapJobRepositoryFactoryBean): JobRepository? {
        return factory.`object`
    }

    @Bean
    fun jobLauncher(jobRepository: JobRepository): SimpleJobLauncher {
        val launcher = SimpleJobLauncher()
        launcher.setJobRepository(jobRepository)
        return launcher
    }

}