package com.sageburner.ingest.batch.pickticket

import org.slf4j.LoggerFactory
import org.springframework.beans.factory.DisposableBean
import org.springframework.beans.factory.InitializingBean
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.core.env.Environment
import org.springframework.web.client.RestTemplate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

@SpringBootApplication
class PickTicketBatchIngest : InitializingBean, DisposableBean {

    private val log = LoggerFactory.getLogger(PickTicketBatchIngest::class.java)

    @Autowired
    private lateinit var env: Environment

    override fun afterPropertiesSet() {
        log.info("Application Version: " + env.getProperty("app.version"))
        configureSSLContext()
    }

    override fun destroy() {
        log.info("Destroying ...")
    }

    @Bean
    fun restTemplate(builder: RestTemplateBuilder): RestTemplate {
        return builder.build()
    }

    private fun configureSSLContext() {
        try {
            val ctx = SSLContext.getInstance("TLS")
            val tm = object : X509TrustManager {
                override fun checkClientTrusted(p0: Array<out java.security.cert.X509Certificate>?, p1: String?) { }

                override fun checkServerTrusted(p0: Array<out java.security.cert.X509Certificate>?, p1: String?) { }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return emptyArray()
                }
            }

            ctx.init(null, arrayOf<TrustManager>(tm), null)

            SSLContext.setDefault(ctx)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
    }


}

fun main(args: Array<String>) {
    runApplication<PickTicketBatchIngest>(*args)
}
