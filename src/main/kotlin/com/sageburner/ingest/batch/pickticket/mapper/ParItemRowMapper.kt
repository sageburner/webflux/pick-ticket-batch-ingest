package com.sageburner.ingest.batch.pickticket.mapper

import com.sageburner.ingest.batch.pickticket.model.ParItem
import org.springframework.jdbc.core.RowMapper
import org.springframework.stereotype.Component
import java.sql.ResultSet

@Component
class ParItemRowMapper : RowMapper<ParItem> {
    override fun mapRow(rs: ResultSet, rowNum: Int): ParItem {
        return ParItem(
                rs.getString(1),
                "",
                rs.getString(2),
                rs.getString(3),
                rs.getString(4),
                rs.getDouble(5),
                rs.getString(6)
        )
    }

}
