package com.sageburner.ingest.batch.pickticket.configuration

import com.sageburner.ingest.batch.pickticket.model.IngestItem
import com.sageburner.ingest.batch.pickticket.model.ParItem
import com.sageburner.ingest.batch.pickticket.processor.IngestToParItemProcessor
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.JobScope
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepScope
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider
import org.springframework.batch.item.database.JdbcBatchItemWriter
import org.springframework.batch.item.file.FlatFileItemReader
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper
import org.springframework.batch.item.file.mapping.DefaultLineMapper
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.UrlResource
import javax.sql.DataSource

@Configuration
class FileIngestConfiguration {

    @Autowired
    lateinit var dataSource: DataSource

    @Autowired
    lateinit var stepBuilderFactory: StepBuilderFactory

    @Bean
    fun importParItemStep(): Step {
        return stepBuilderFactory.get("importParItemStep")
                .chunk<IngestItem, ParItem>(10)
                .reader(flatFileReader("", "", ""))
                .processor(processor())
                .writer(writer())
                .build()
    }

    @Bean
    @StepScope
    fun flatFileReader(
            @Value("#{jobParameters['fileBaseDir']}") baseDir: String,
            @Value("#{jobParameters['backupFileDir']}") backupDir: String,
            @Value("#{jobParameters['backupFileName']}") fileName: String): FlatFileItemReader<IngestItem> {
        val reader: FlatFileItemReader<IngestItem> = FlatFileItemReader()
        reader.setResource(UrlResource("file:$baseDir/$backupDir/$fileName" ))
        reader.setLinesToSkip(1)

        val tokenizer = DelimitedLineTokenizer()
        tokenizer.setDelimiter(",")
        tokenizer.setNames(
                "itemNumber",
                "externalItemNumber",
                "sku",
                "description",
                "longDescription",
                "source",
                "price",
                "imageUrl",
                "active",
                "note")

        val fieldSetMapper: BeanWrapperFieldSetMapper<IngestItem> = BeanWrapperFieldSetMapper()
        fieldSetMapper.setTargetType(IngestItem::class.java)

        val lineMapper: DefaultLineMapper<IngestItem> = DefaultLineMapper()
        lineMapper.setLineTokenizer(tokenizer)
        lineMapper.setFieldSetMapper(fieldSetMapper)

        reader.setLineMapper(lineMapper)

        return reader
    }

    @Bean
    fun processor(): IngestToParItemProcessor {
        return IngestToParItemProcessor()
    }

    @Bean
    fun writer(): JdbcBatchItemWriter<ParItem> {
        val writer: JdbcBatchItemWriter<ParItem> = JdbcBatchItemWriter()
        writer.setItemSqlParameterSourceProvider(BeanPropertyItemSqlParameterSourceProvider())
        writer.setSql("INSERT INTO par_item " +
                "(id, parInventoryId, externalId, description, source, price, note) " +
                "VALUES " +
                "(:id, :parInventoryId, :externalId, :description, :source, :price, :note)")
        writer.setDataSource(dataSource)
        return writer
    }

}
