package com.sageburner.ingest.batch.pickticket.scheduler

import org.slf4j.LoggerFactory
import org.springframework.batch.core.Job
import org.springframework.batch.core.JobExecution
import org.springframework.batch.core.JobParameters
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.support.SimpleJobLauncher
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

@Configuration
@EnableScheduling
class FlowJobScheduler {

    private val log = LoggerFactory.getLogger(FlowJobScheduler::class.java)

    @Autowired
    private lateinit var env: Environment

    @Autowired
    lateinit var jobLauncher: SimpleJobLauncher

    @Autowired
    lateinit var flowJob: Job

    @Scheduled(cron = "0 */30 * * * *")
    fun perform() {
        val theTime = LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)

        val fileName = env.getProperty("app.files.fileName")
        val fileBaseDir = env.getProperty("app.files.baseLocation")
        val inFileDir = env.getProperty("app.files.incomingDir")
        val backupFileDir = env.getProperty("app.files.backupDir")


        log.info("Processing")
        log.info("Job started at : $theTime")

        val params: JobParameters = JobParametersBuilder()
                .addString("JobID", "flowJob-$theTime")
                .addString("inFileName", fileName)
                .addString("backupFileName", "$fileName-$theTime")
                .addString("fileBaseDir", fileBaseDir)
                .addString("inFileDir", inFileDir)
                .addString("backupFileDir", backupFileDir)
                .toJobParameters()

        val execution: JobExecution = jobLauncher.run(flowJob, params)

        println("Job finished with status : " + execution.status)
    }
}
