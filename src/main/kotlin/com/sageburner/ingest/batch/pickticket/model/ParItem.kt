package com.sageburner.ingest.batch.pickticket.model

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * id -> ITEM_NUMBER [0]
 * externalId -> EXT_ITEM_NUMBER [1]
 * description -> DESCRIPTION [3]
 * source -> SOURCE [5]
 * price -> PRICE [6]
 * note -> NOTE [9]
 */
@NoArg
data class ParItem(
        @JsonIgnore val id: String,
        val parInventoryId: String? = "",
        val externalId: String? = "",
        @JsonProperty("desc") val description: String? = "",
        @JsonIgnore val source: String,
        val price: Double? = 0.0,
        val note: String? = "")
