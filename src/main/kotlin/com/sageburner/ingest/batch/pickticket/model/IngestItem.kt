package com.sageburner.ingest.batch.pickticket.model

@NoArg
data class IngestItem(
        var itemNumber: String,
        var externalItemNumber: String? = "",
        var sku: String? = "",
        var description: String? = "",
        var longDescription: String? = "",
        var source: String = "",
        var price: Double? = 0.0,
        var imageUrl: String? = "",
        var active: String? = "",
        var note: String? = "")
