package com.sageburner.ingest.batch.pickticket.tasklet

import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.stereotype.Component

@Component
class DataStoreCleanupTasklet : Tasklet {

    private val log = LoggerFactory.getLogger(DataStoreCleanupTasklet::class.java)

    @Autowired
    lateinit var jdbcTemplate: JdbcTemplate

    override fun execute(contribution: StepContribution, chunkContext: ChunkContext): RepeatStatus {
        log.info("TRUNCATING LOCAL DATA STORE >>>")

        jdbcTemplate.execute("TRUNCATE TABLE par_item")

        return RepeatStatus.FINISHED
    }
}