package com.sageburner.ingest.batch.pickticket.processor

import com.sageburner.ingest.batch.pickticket.model.IngestItem
import com.sageburner.ingest.batch.pickticket.model.ParItem
import org.springframework.batch.item.ItemProcessor
import java.util.*
import org.slf4j.LoggerFactory



class IngestToParItemProcessor : ItemProcessor<IngestItem, ParItem> {

    private val log = LoggerFactory.getLogger(IngestToParItemProcessor::class.java)

    override fun process(item: IngestItem?): ParItem {
        return item?.let {
            ParItem(
                    id = it.itemNumber,
                    externalId = it.externalItemNumber,
                    description = it.description,
                    source = it.source,
                    price = it.price,
                    note = it.note
            )
        } ?: ParItem(id = UUID.randomUUID().toString(), source = "")
    }

}
