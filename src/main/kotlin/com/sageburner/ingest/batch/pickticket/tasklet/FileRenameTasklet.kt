package com.sageburner.ingest.batch.pickticket.tasklet

import org.slf4j.LoggerFactory
import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.stereotype.Component
import java.io.File

@Component
class FileRenameTasklet : Tasklet {

    private val log = LoggerFactory.getLogger(FileRenameTasklet::class.java)

    override fun execute(contribution: StepContribution, chunkContext: ChunkContext): RepeatStatus {
        log.info("RENAMING INGEST FILE >>>")

        val jobParams = chunkContext.stepContext.jobParameters

        val inFileName = jobParams["inFileName"]
        val backupFileName = jobParams["backupFileName"]
        val fileBaseDir = jobParams["fileBaseDir"]
        val inFileDir = jobParams["inFileDir"]
        val backupFileDir = jobParams["backupFileDir"]

        val inFile = File("$fileBaseDir/$inFileDir/$inFileName")
        if ( inFile.isFile && inFile.canRead() ) {
            inFile.renameTo(File("$fileBaseDir/$backupFileDir/$backupFileName"))
        } else {
            log.info("NO INGEST FILE FOUND - CANCELLING JOB >>>")
            chunkContext.stepContext.stepExecution.jobExecution.stop()
        }

        return RepeatStatus.FINISHED
    }
}