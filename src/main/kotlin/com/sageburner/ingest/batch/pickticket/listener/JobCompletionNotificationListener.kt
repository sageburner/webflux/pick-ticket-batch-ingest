package com.sageburner.ingest.batch.pickticket.listener

import org.slf4j.LoggerFactory
import org.springframework.batch.core.BatchStatus
import org.springframework.batch.core.JobExecution
import org.springframework.batch.core.listener.JobExecutionListenerSupport
import org.springframework.boot.SpringApplication
import org.springframework.stereotype.Component
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext


@Component
class JobCompletionNotificationListener: JobExecutionListenerSupport() {

    private val log = LoggerFactory.getLogger(JobCompletionNotificationListener::class.java)

    @Autowired
    lateinit var context: ApplicationContext

    override fun afterJob(jobExecution: JobExecution) {
        if(jobExecution.status == BatchStatus.COMPLETED) {
            log.info("JOB COMPLETE >>>")
        }
    }
}
