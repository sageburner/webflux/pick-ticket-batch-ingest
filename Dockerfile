FROM frolvlad/alpine-oraclejdk8:slim
VOLUME /opt/app/working-files
ENV spring_profiles_active=default
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/opt/app/app.jar"]
